/*
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.detls.request;

import com.dtflys.forest.annotation.Get;
import com.dtflys.forest.annotation.Query;
import com.dtflys.forest.annotation.Request;

public interface WebToolsApi {


    final String app_id = "rgihdrm0kslojqvm";
    final String app_secret = "WnhrK251TWlUUThqaVFWbG5OeGQwdz09";
    @Request(
            url = "https://cuonc.com/api/icp/",
            headers = "Accept: text/plain"
    )
    String sendRequest(@Query("domain") String domain);

    @Request(
            url = "https://api.uomg.com/api/ck_qiang",
            headers = "Accept: text/plain"
    )
    String sendRequestDomainStats(@Query("domain") String domain);

    @Get(
            url = "https://www.mxnzp.com/api/weather/current/{0}?app_id=rgihdrm0kslojqvm&app_secret=WnhrK251TWlUUThqaVFWbG5OeGQwdz09"
    )
    String RequestWeatherStats(String city);

    @Request(
            url = "https://cuonc.com/api/video/jxindex.php"
    )
    String RequestVideoDownload(@Query("url") String url);


    @Request(
            url = "https://api.iyk0.cn/api/video_jx",
            userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36",
            contentType = "application/x-www-form-urlencoded"
    )
    String RequestVideoDownloadV2(@Query("url") String url);

    @Request(
            url = "https://api.iyk0.cn/api/oil_prices",
            userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36",
            contentType = "application/x-www-form-urlencoded"
    )
    String RequestOilPrices(@Query("shengfen") String shengfen);
}
