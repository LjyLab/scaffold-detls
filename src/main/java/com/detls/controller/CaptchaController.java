package com.detls.controller;

import com.detls.config.exception.RunException;
import com.detls.config.result.R;
import com.detls.config.timeout.TakeCount;
import com.detls.util.EncryptUtil;
import com.detls.util.IpUtils;
import com.detls.config.result.Result;
import com.wf.captcha.SpecCaptcha;
import com.wf.captcha.utils.CaptchaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @author XiaoJun
 */
@RestController
public class CaptchaController {
    @Autowired
    private StringRedisTemplate redisTemplate;

    private static final long CAPTCHA_CODE_EXPIRED = 30;
    @TakeCount
    @ResponseBody
    @RequestMapping("/captcha")
    public R getCaptcha(HttpServletRequest request, HttpServletResponse response) throws Exception {
        SpecCaptcha specCaptcha = new SpecCaptcha(130, 48, 4);
        String verCode = specCaptcha.text().toLowerCase();
        // 设置获取key
        String key = getCaptchaKey(request);
        // 存入redis并设置过期时间为30分钟
        redisTemplate.opsForValue().set(key,verCode,CAPTCHA_CODE_EXPIRED, TimeUnit.MINUTES);
        // 将key和base64返回给前端
        return R.ok().data("key",key).data("captcha",specCaptcha.toBase64());
    }

    /**
     * 获取缓存的key
     * @param request
     * @return
     */
    private String getCaptchaKey(HttpServletRequest request){
        String ip = IpUtils.getIpAddr(request);
        // 获取浏览器请求头
        String userAgent = request.getHeader("User-Agent");
        String key = "user-service:captcha:"+ EncryptUtil.MD5(ip+userAgent);
        return key;
    }

    @GetMapping("/v2/captcha")
    public void captchaV2(String key, HttpServletRequest request, HttpServletResponse response) throws Exception{
        CaptchaUtil captcha = new CaptchaUtil();
        try {
            captcha.out(Integer.parseInt(key), request, response);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @TakeCount
    @GetMapping("/")
    public R HelloWorld(){
        return R.ok().message("服务正常").data("Server","Server Success")
                .data("Jenkins","Jenkins Success")
                .data("Tengine","Tengine Success")
                .data("Redis","Redis Success");
    }

//    @GetMapping("/error_{status}")
//    public R errorPage(@PathVariable("status") Integer status, HttpServletRequest request) {
//
//        String ipAdd = IpUtils.getIpAddr(request);
//        if (status == HttpStatus.NOT_FOUND.value()) {
//            return R.error().code(status).message(HttpStatus.NOT_FOUND.getReasonPhrase())
//                    .data("IpAddress",ipAdd).data("Tips","是不是搞错了?这个接口不存在");
//        }
//        if (status == HttpStatus.INTERNAL_SERVER_ERROR.value()){
//            return R.error().code(status).message(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
//                    .data("IpAddress",ipAdd).data("Tips","服务器内部错误，请稍后重试");
//        }
//        //错误信息返回
//        return R.error().code(status).message("抱歉当前页面丢失")
//                .data("IpAddress",ipAdd).data("ErrorCode",status);
//    }

}
