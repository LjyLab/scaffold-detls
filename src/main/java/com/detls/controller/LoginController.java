/*
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.detls.controller;

import com.detls.config.constant.CommonConstant;
import com.detls.config.exception.Assert;
import com.detls.config.exception.ValidatorUtil;
import com.detls.config.result.R;
import com.detls.config.result.Result;
import com.detls.config.timeout.TakeCount;
import com.detls.dto.LoginDTO;
import com.detls.dto.RegisterDTO;
import com.detls.entity.SysUser;
import com.detls.service.ISysUserService;
import com.detls.util.CommonMapper;
import com.detls.util.*;
import com.alibaba.fastjson.JSONObject;
import com.detls.vo.SysUserVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/sys")
@Slf4j
public class LoginController {
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private StringRedisTemplate redisTemplate;
    private static final long PASSWORD_LENGTH = 8;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Result login(@RequestBody LoginDTO loginUser) {
        ValidatorUtil.validateEntity(loginUser);
        Result<JSONObject> result = new Result<JSONObject>();
        String username = loginUser.getUserName();
        String password = loginUser.getPassWord();
        //1. 校验用户是否有效
        SysUser sysUser = sysUserService.getUserByName(username);
        result = sysUserService.checkUserIsEffective(sysUser);
        if (!result.isSuccess()) {
            return result;
        }
        //2. 校验用户名或密码是否正确
        String syspassword = sysUser.getPassWord();
        String userName = PasswordUtil.decrypt(syspassword, password, sysUser.getSalt());
        Assert.verify(null == userName||!username.equals(userName),"用户名或密码错误");

        Object o = redisTemplate.opsForValue().get(loginUser.getKey());
        Assert.isNull(o,"验证码已过期");
        Assert.verify(!loginUser.getCode().equalsIgnoreCase(o.toString()),"验证码错误");

        //用户登录信息
        userInfo(sysUser, result);
        //登录成功删除key
        redisTemplate.delete(loginUser.getKey());
        return result;
    }

    @TakeCount
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public R register(@RequestBody RegisterDTO dto) {
        ValidatorUtil.validateEntity(dto);
        //未提交Key的提示
        Assert.isNull(dto.getKey(),"Key is Null");
        //拿到验证码匹配
        Object o = redisTemplate.opsForValue().get(dto.getKey());
        Assert.isNull(o,"验证码已过期");
        SysUser sysUser = sysUserService.getUserByName(dto.getUserName());
        Assert.verify(null!=sysUser,"用户名已存在，请重新输入!");
        sysUser = new SysUser();
        sysUser.setUserName(dto.getUserName());

        Assert.verify(dto.getPassWord().length() < PASSWORD_LENGTH,"密码长度不能低于8位");
        String salt = SaltUtil.getSalt(8);
        sysUser.setSalt(salt);
        String encryptPass = PasswordUtil.encrypt(dto.getUserName(), dto.getPassWord(), salt);
        sysUser.setPassWord(encryptPass);

        sysUser.setRealName(dto.getRealName());
        int i = sysUserService.register(sysUser);
        Assert.verify(i == 0,"注册失败");
        return R.ok().message("注册成功");
    }


    /**
     * 用户信息
     *
     * @param sysUser
     * @param result
     * @return
     */
    private Result<JSONObject> userInfo(SysUser sysUser, Result<JSONObject> result) {
        String syspassword = sysUser.getPassWord();
        String username = sysUser.getUserName();
        // 生成token
        String token = JwtUtil.sign(username, syspassword);
        redisUtil.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
        // 设置超时时间
        redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME / 1000);
        // 获取用户部门信息
        JSONObject obj = new JSONObject();
//        SysUserVO sysUserVO = SysUserMapper.INSTANCE.UserToUserVO(sysUser);
        SysUserVO sysUserVO = CommonMapper.INSTANCE.userToUserVo(sysUser);
        obj.put("info", sysUserVO);
        result.setResult(obj);
        obj.put("token", token);
        result.success("登录成功");
        return result;
    }

    /**
     * 退出登录
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/logout",method = RequestMethod.POST)
    public Result<Object> logout(HttpServletRequest request, HttpServletResponse response) {
        //用户退出逻辑
        String token = request.getHeader(CommonConstant.ACCESS_TOKEN);
        if (CommonUtils.isEmpty(token)) {
            return Result.error("退出登录失败！");
        }
        String username = JwtUtil.getUsername(token);
        SysUser sysUser = sysUserService.getUserByName(username);
        if (sysUser != null) {
            log.info(" 用户名:  " + sysUser.getRealName() + ",退出成功！ ");
            //清空用户Token缓存
            redisUtil.del(CommonConstant.PREFIX_USER_TOKEN + token);
            //清空用户权限缓存：权限Perms和角色集合
            redisUtil.del(CommonConstant.LOGIN_USER_CACHERULES_ROLE + username);
            redisUtil.del(CommonConstant.LOGIN_USER_CACHERULES_PERMISSION + username);
            return Result.ok("退出登录成功！");
        } else {
            return Result.error("无效的token");
        }
    }

}
