/*
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.detls.controller;



import cn.hutool.core.date.BetweenFormatter;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.detls.util.DateUtils;
import com.detls.util.SaltUtil;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.LoadLibs;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TestController {
    public static void main(String[] args) {

        long t = DateTime.now().getTime();


        String dateStr = "2022-11-02 00:00:00";
        Date date = DateUtil.parse(dateStr);
        System.out.println(date);


        long dateTimeNo = DateUtils.getDateNo(date);

        System.out.println("时间"+dateTimeNo);

        String dateStr1 = "2022-11-01 00:00:00";
        Date date1 = DateUtil.parse(dateStr1);

        String dateStr2 = "2022-11-01 00:00:00";
        Date date2 = DateUtil.parse(dateStr2);

        //相差一个月，31天
        long betweenDay = DateUtil.between(date1, date2, DateUnit.DAY);
        System.out.println(betweenDay);



        //计算还剩多少天
        String formatBetween = DateUtil.formatBetween(dateTimeNo, BetweenFormatter.Level.MINUTE);
        System.out.println(formatBetween);


        System.out.println(DateTime.now().getTime());
        System.out.println(DateTime.of(t));
    }
}
class Test01{
    public static void main(String[] args) {
        String dateStr1 = "2022-11-01 01:00:00";
        Date date1 = DateUtil.parse(dateStr1);
        String dateStr2 = "2022-11-02 01:00:00";
        Date date2 = DateUtil.parse(dateStr2);

        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        long timestamp = cal.getTimeInMillis();

        //时间戳转换为时间
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = sdf.format(timestamp);


        long betweenDay = DateUtil.between(date1, DateUtil.date(), DateUnit.DAY);



        System.out.println(betweenDay);
        System.out.println(format);

    }
}
class Test02{
    public static void main(String[] args) {
        String dateStr1 = "2022-11-01 01:00:00";
        Date date1 = DateUtil.parse(dateStr1);

        Calendar instance = Calendar.getInstance();
        instance.setTime(date1);
        instance.add(Calendar.DATE,7);
        Date date = instance.getTime();
        String format = DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");

        System.out.println(format);
    }
}
class Test03{
    public static void main(String[] args) {
        System.out.println(SaltUtil.getLetterPsw("HD",20));
        System.out.println(SaltUtil.getLetterPswSalt("HD",20));
    }
}
class Test04{
    public static void main(String[] args) {
        DateTime time = DateUtil.date().setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
        System.out.println("现在时间：" + time);
        Calendar instance = Calendar.getInstance();
        instance.setTime(time);
        instance.add(Calendar.DATE,3);
        Date date = instance.getTime();
        String format = DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
        System.out.println(date);
        System.out.println(format);
    }
}
