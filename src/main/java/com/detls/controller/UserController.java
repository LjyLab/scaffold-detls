package com.detls.controller;

import com.detls.config.result.R;
import com.detls.config.result.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @RequiresPermissions("user:list")
    @RequestMapping("list")
    public Result userList() {
        return Result.ok("获取用户信息");
    }

    @RequiresPermissions("user:add")
    @RequestMapping("add")
    public Result userAdd() {
        return Result.ok("新增用户");
    }

    @RequiresPermissions("user:delete")
    @RequestMapping("delete")
    public Result userDelete() {
        return Result.ok("删除用户");
    }

    @RequiresPermissions("user:update")
    @RequestMapping("update")
    public R userUpdate() {
        return R.ok().data("tips","更新用户");
    }

    @RequestMapping("test")
    public Result test() {
        return Result.ok("不用登陆直接访问的接口");
    }
}
