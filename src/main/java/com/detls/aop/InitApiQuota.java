/*
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.detls.aop;

import com.detls.entity.SysKey;
import com.detls.mapper.SysKeyMapper;
import com.detls.util.RedisUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

//@Component
public class InitApiQuota implements CommandLineRunner {

    @Resource
    private SysKeyMapper sysKeyMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void run(String... args) throws Exception {
        List<SysKey> sysKeys = sysKeyMapper.selectAll();
        sysKeys.forEach(sysKey -> stringRedisTemplate.opsForValue().set(sysKey.getKey(),sysKey.getQuota().toString()));
    }
}
