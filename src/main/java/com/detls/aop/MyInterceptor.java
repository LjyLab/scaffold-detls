/*
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.detls.aop;

import cn.hutool.core.lang.Assert;
import cn.hutool.json.JSONObject;
import com.detls.config.constant.CacheConstant;
import com.detls.util.SpringContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Map;

@Slf4j
public class MyInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("拦截请求：" + request.getRequestURL());
        Map<String, String[]> parameterMap = request.getParameterMap();
        Assert.isTrue(parameterMap.size()>0,"缺失必要参数");
        String[] keys = parameterMap.get("key");
        Assert.isTrue((keys != null),"缺少KEY参数");
        log.info("请求参数 Key：" + keys[0]);
        StringRedisTemplate stringRedisTemplate = (StringRedisTemplate)SpringContextUtils.getBean("stringRedisTemplate");
        String quotaStr = stringRedisTemplate.opsForValue().get(CacheConstant.KEY_QUOTA + keys[0]);
        Integer quota = 0;

        if (null!=quotaStr){
            quota = Integer.valueOf(quotaStr);
            log.info(keys[0] + "接口访问次数剩余：" + quota);
        }
        if(quota > 0){
            log.error("接口访问成功");
            return true;
        }
        log.error("访问拒绝/次数用尽");
        JSONObject jsonObject = new JSONObject();
        jsonObject.set("code", 400);
        jsonObject.set("msg", "访问被拒绝");
        jsonObject.set("tips","该KEY不存在 或 额度已用尽");
        returnJson(response,jsonObject.toString());
        return false;

    }

    private void returnJson(HttpServletResponse response, String json) throws Exception{
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=utf-8");
        PrintWriter writer = response.getWriter();
        writer.print(json);
        writer.close();
    }
}
