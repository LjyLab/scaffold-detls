package com.detls.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

@Data
public class SysKeyExpire implements Serializable {
    private Integer id;

    private String name;

    private String nickname;

    private String masterqq;

    private String robotqq;

    private String key;

    private Integer keyid;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone ="GMT+8")
    private Date start;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone ="GMT+8")
    private Date expire;

    private Integer quota;

    private static final long serialVersionUID = 1L;
}