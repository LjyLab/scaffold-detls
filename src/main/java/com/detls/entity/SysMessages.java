/*
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.detls.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.detls.dto.SysLabelTarget;
import lombok.Data;

@Data
public class SysMessages {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String uuid;

    private String hitokoto;

    private String type;

    @TableField("`from`")
    private String from;

    private String creator;

    private String length;

    @TableField(exist = false)
    private SysLabelTarget sysLabelTarget;
}