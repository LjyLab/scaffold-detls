/*
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.detls.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class SysKey implements Serializable {
    private Integer id;

    private Integer userId;

    private Integer roleId;

    @TableField("`key`")
    private String key;

    private Date start;

    private Date expire;

    private Integer quota;

    private SysUser sysUser;

    private static final long serialVersionUID = 1L;
}