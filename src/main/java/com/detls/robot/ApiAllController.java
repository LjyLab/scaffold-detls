/*
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.detls.robot;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.net.NetUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.detls.config.constant.CacheConstant;
import com.detls.config.exception.Assert;
import com.detls.config.result.R;
import com.detls.config.timeout.TakeCount;
import com.detls.entity.SysKeyExpire;
import com.detls.mapper.SysKeyExpireMapper;
import com.detls.mapper.SysKeyMapper;
import com.detls.request.WebToolsApi;
import com.github.hiwepy.ip2region.spring.boot.IP2regionTemplate;
import com.github.hiwepy.ip2region.spring.boot.ext.RegionAddress;
import com.github.hiwepy.ip2region.spring.boot.ext.RegionEnum;
import lombok.extern.slf4j.Slf4j;
import me.ihxq.projects.pna.PhoneNumberInfo;
import me.ihxq.projects.pna.PhoneNumberLookup;
import net.dreamlu.mica.ip2region.core.Ip2regionSearcher;
import net.dreamlu.mica.ip2region.core.IpInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

@RestController
@RequestMapping("/api")
@Slf4j
public class ApiAllController {
    @Autowired
    private IP2regionTemplate ip;
    @Autowired
    private Ip2regionSearcher regionSearcher;
    @Autowired
    private SysKeyMapper sysKeyMapper;
    @Autowired
    private SysKeyExpireMapper sysKeyExpireMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private WebToolsApi webToolsApi;
    @Resource
    private KeyExpireComponent keyExpireComponent;

    @TakeCount
    @GetMapping
    public R apiIndexPage(){
        return R.ok();
    }

    @TakeCount
    @GetMapping("/domain")
    public R DomainGetIp(String domain,String key) throws IOException {
        keyExpireComponent.RequestKeyByCheck(domain,key);
        String ipByHost = NetUtil.getIpByHost(domain);
        RegionAddress regionAddress = ip.getRegionAddress(ipByHost);
        RegionEnum byRegionAddress = RegionEnum.getByRegionAddress(regionAddress);
        return R.ok().data("domain",domain)
                .data("host",ipByHost)
                .data("region",regionAddress)
                .data("address",byRegionAddress)
                .data("nation",ip.getCountryByIp(ipByHost))
                .data("user",keyExpireComponent.ReckonTimeByKeyMaturity(key));
    }

    @TakeCount
    @GetMapping("/domain/v2")
    public R DomainGetIpV2(String domain,String key) throws IOException {
        Integer exist = sysKeyMapper.selectByKey(key);
        Assert.isNull(key,"参数缺失(Key is Null)");
        Assert.verify(exist == null,"密钥错误(key is error)");
        Assert.verify(!keyExpireComponent.ReckonTimeByKeyStatus(key),"秘钥已过期，请重新申请(The secret key expired)");
        Assert.verify(domain == null || "".equalsIgnoreCase(domain),"参数缺失(Domain is Null),请输入正确的域名");
        String ipByHost = NetUtil.getIpByHost(domain);
        IpInfo ipInfo = regionSearcher.memorySearch(ipByHost);
        return R.ok().data("domain",domain)
                .data("host",ipByHost)
                .data("info",ipInfo)
                .data("user",keyExpireComponent.ReckonTimeByKeyMaturity(key));
    }

    @TakeCount
    @GetMapping("/icp")
    public R DomainGetIcp(String domain,String key){
        keyExpireComponent.RequestKeyByCheck(domain,key);
        Integer exist = sysKeyMapper.selectByKey(key);
        Assert.isNull(key,"参数缺失(Key is Null)");
        Assert.verify(exist == null,"密钥错误(key is error)");
        Assert.verify(!keyExpireComponent.ReckonTimeByKeyStatus(key),"秘钥已过期，请重新申请(The secret key expired)");
        if (domain != null){
            Assert.verify("".equalsIgnoreCase(domain),"参数缺失(Domain is Null),请输入正确的域名");
            String result = webToolsApi.sendRequest(domain);
            JSONObject jsonObject = JSON.parseObject(result);
            JSONObject info = new JSONObject();
            info.put("icp",jsonObject.getString("icp"));
            info.put("unit",jsonObject.getString("unitName"));
            info.put("nature",jsonObject.getString("natureName"));
            return R.ok().data("domain",domain).data("info",info)
                    .data("user",keyExpireComponent.ReckonTimeByKeyMaturity(key));
        }
        return R.error().data("tips","未知错误");
    }

    @TakeCount
    @GetMapping("/gfw")
    public R DomainGetCkStats(String domain,String key){
        keyExpireComponent.RequestKeyByCheck(domain,key);
        String thisDomain = NetUtil.getIpByHost(domain);
        boolean ping = NetUtil.ping(thisDomain);
        if (ping){
            return R.ok().data("domain",domain).data("status", true).data("tips","域名一切正常哦~")
                    .data("user",keyExpireComponent.ReckonTimeByKeyMaturity(key));
        }else {
            return R.ok().data("domain",domain).data("status", false).data("tips","域名被墙啦~你怎么搞的嘛！")
                    .data("user",keyExpireComponent.ReckonTimeByKeyMaturity(key));
        }
    }

    @TakeCount
    @GetMapping("/location")
    public R NumberInfo(String number,String key){
        keyExpireComponent.RequestKeyByCheck(number,key);
        PhoneNumberLookup phoneNumberLookup = new PhoneNumberLookup();
        PhoneNumberInfo found = phoneNumberLookup.lookup(number).orElseThrow(RuntimeException::new);
        found.getNumber(); // 18798896741
        found.getAttribution().getProvince(); // 贵州
        found.getAttribution().getCity(); // 贵阳
        found.getAttribution().getZipCode(); // 550000
        found.getAttribution().getAreaCode(); // 0851
        found.getIsp(); // ISP.CHINA_MOBILE
        return R.ok().data("info",found).data("operator",found.getIsp().getCnName())
                .data("user",keyExpireComponent.ReckonTimeByKeyMaturity(key));
    }

    @TakeCount
    @GetMapping("/weather")
    public R WeatherStats(String city,String key){
        keyExpireComponent.RequestKeyByCheck(city,key);
        String result = webToolsApi.RequestWeatherStats(city);
        JSONObject jsonObject = JSON.parseObject(result);
        JSONObject infoJson = jsonObject.getJSONObject("data");
        JSONObject info = new JSONObject();
        info.put("address",infoJson.getString("address"));
        info.put("cityCode",infoJson.getString("cityCode"));
        info.put("temp",infoJson.getString("temp"));
        info.put("weather",infoJson.getString("weather"));
        info.put("windDirection",infoJson.getString("windDirection"));
        info.put("windPower",infoJson.getString("windPower"));
        info.put("humidity",infoJson.getString("humidity"));
        return R.ok().data("info",info).data("user",keyExpireComponent.ReckonTimeByKeyMaturity(key));
    }

    @TakeCount
    @GetMapping("/video")
    public R RequestVideoDownload(String url,String key){
        keyExpireComponent.RequestKeyByCheck(url,key);
        String result = webToolsApi.RequestVideoDownload(url);
        JSONObject jsonObject = JSON.parseObject(result);
        JSONObject infoJson = jsonObject.getJSONObject("message");
        JSONObject info = new JSONObject();
        info.put("nickname",infoJson.getString("nickname"));
        info.put("videoUrl",infoJson.getString("video_url"));
        info.put("music",infoJson.getString("music"));
        info.put("type",infoJson.getString("type"));
        return R.ok().data("info",info).data("user",keyExpireComponent.ReckonTimeByKeyMaturity(key));
    }

    @TakeCount
    @GetMapping("/video/v2")
    public R RequestVideoDownloadV2(String url,String key){
        keyExpireComponent.RequestKeyByCheck(url,key);
        String result = webToolsApi.RequestVideoDownloadV2(url);
        JSONObject jsonObject = JSON.parseObject(result);
        JSONObject infoJson = jsonObject.getJSONObject("data");
        JSONObject info = new JSONObject();
        info.put("author",infoJson.getString("author"));
        info.put("title",infoJson.getString("title"));
        info.put("cover",infoJson.getString("cover"));
        info.put("url",infoJson.getString("url"));
        return R.ok().data("info",info).data("user",keyExpireComponent.ReckonTimeByKeyMaturity(key));
    }

    @TakeCount
    @GetMapping("/oil")
    public R RequestOilPrices(String province,String key){
        keyExpireComponent.RequestKeyByCheck(province,key);
        //秘钥检测
        String result = webToolsApi.RequestOilPrices(province);
        JSONObject jsonObject = JSON.parseObject(result);
        JSONArray data = jsonObject.getJSONArray("data");
        return R.ok().data("info",data).data("user",keyExpireComponent.ReckonTimeByKeyMaturity(key));
    }

    @TakeCount
    @GetMapping("/key")
    public R RequestKeyExpireDate(String key){
        Assert.verify(!keyExpireComponent.ReckonTimeByKeyStatus(key),"账号已过期");
        return R.ok().data("status",keyExpireComponent.ReckonTimeByKeyStatus(key))
                .data("user",keyExpireComponent.ReckonTimeByKeyMaturity(key));
    }

}
