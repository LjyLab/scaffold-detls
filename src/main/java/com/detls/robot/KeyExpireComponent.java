/*
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.detls.robot;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.detls.config.constant.CacheConstant;
import com.detls.config.exception.Assert;
import com.detls.entity.SysKeyExpire;
import com.detls.mapper.SysKeyExpireMapper;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

@Component
public class KeyExpireComponent {

    @Resource
    private SysKeyExpireMapper sysKeyExpireMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    public void RequestKeyByCheck(String parameter,String key){
        Integer exist = sysKeyExpireMapper.selectByKey(key);
        Assert.isNull(key,"参数缺失(Key is Null)");
        Assert.verify(exist == null,"密钥错误(key is error)");
        Assert.verify(!ReckonTimeByKeyStatus(key),"秘钥已过期，请重新申请或续费使用(The secret key expired)");
        Assert.verify(parameter == null || "".equalsIgnoreCase(parameter),"参数缺失(Parameter is Null),请输入正确的请求参数");
    }

    boolean ReckonTimeByKeyStatus(String key){
        String date = sysKeyExpireMapper.selectByExpireDate(key);
        Date userTime = DateUtil.parse(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(userTime);
        long timestamp = cal.getTimeInMillis();
        //当前时间减去用户的时间 没有则小于0
        if (DateUtil.current() - timestamp <= 0){
            return true;
        }
        return false;
    }

    @GetMapping("/info")
    public SysKeyExpire RequestKeyByInfo(String key){
//        SysKey sysKey = sysKeyMapper.selectByUserKeyInfo(key);
        SysKeyExpire sysKeyExpire = sysKeyExpireMapper.selectByKeyExpireInfo(key);
//        Date expire = sysKey.getExpire();
//        BeanCopy<SysKey, SysUserKeyVO> beanCopy = new BeanCopy();
//        SysUserKeyVO convert = beanCopy.convert(sysKey, SysUserKeyVO.class);
        return sysKeyExpire;
    }

    public HashMap<String,Object> ReckonTimeByKeyMaturity(String key){
        HashMap<String, Object> map = new HashMap<>();
        String date = sysKeyExpireMapper.selectByExpireDate(key);
        Date userTime = DateUtil.parse(date);
        long betweenDay = DateUtil.between(userTime, DateUtil.date(), DateUnit.DAY);
        map.put("active",stringRedisTemplate.opsForValue().get(CacheConstant.KEY_QUOTA + key));
        map.put("tips","授权剩余 " + betweenDay + " 天");
        map.put("info",RequestKeyByInfo(key));
        return map;
    }
}
