/*
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.detls.robot;

import com.detls.config.result.R;
import com.detls.config.timeout.TakeCount;
import com.detls.entity.SysMessages;
import com.detls.service.SysMessagesService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api/msg")
public class MsgController {

    @Autowired
    private SysMessagesService messagesService;
    @Resource
    private KeyExpireComponent keyExpireComponent;

    @TakeCount
    @GetMapping("/id/{id}")
    public R getMessagesById(@PathVariable("id") Integer id, String key) {
        return R.ok().data("msg", messagesService.getMessagesById(id));
    }

    @TakeCount
    @GetMapping("/uuid/{uuid}")
    public R getMessagesByUUID(@PathVariable("uuid") String uuid){
        return R.ok().data("msg",messagesService.getMessagesByUUID(uuid));
    }

    @TakeCount
    @GetMapping("/{type}")
    public R getRandomMessagesByType(@PathVariable("type") String type){
        return R.ok().data("msg",messagesService.getRandomMessagesByType(type));
    }

    @TakeCount
//    @GetMapping
    @GetMapping
    public R getMessages(String key,String type){
        return messagesService.getRandomMessages(key,type);
    }

    @TakeCount
    @GetMapping("/p/{page}/{limit}")
    public R getPages(@PathVariable("page") Integer page,@PathVariable("limit") Integer limit){
        return messagesService.getPage(page,limit);
    }

    @TakeCount
    @GetMapping("/p")
    public R getGoPages(@Param("page") Integer page,@Param("limit") Integer limit){
        return messagesService.getPage(page,limit);
    }

    @TakeCount
    @GetMapping("/page")
    public R getPageByDev(@Param("offset") Integer offset,@Param("limit") Integer limit){
        SysMessages dev = messagesService.getPageByDev(offset, limit);
        return R.ok().data("data",dev);
    }
}
