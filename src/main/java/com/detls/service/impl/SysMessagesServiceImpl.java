/*
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.detls.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.detls.config.exception.Assert;
import com.detls.config.result.R;
import com.detls.dto.SysUserOrKey;
import com.detls.entity.SysKey;
import com.detls.entity.SysMessages;
import com.detls.mapper.SysKeyMapper;
import com.detls.mapper.SysMessagesMapper;
import com.detls.service.SysMessagesService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysMessagesServiceImpl extends ServiceImpl<SysMessagesMapper, SysMessages> implements SysMessagesService {

    @Autowired
    private SysMessagesMapper sysMessagesMapper;
    @Autowired
    private SysKeyMapper sysKeyMapper;

    @Override
    public SysMessages getRandomMessagesByType(String type) {
        return sysMessagesMapper.getRandomMessagesByType(type);
    }

    @Override
    public SysMessages getMessagesById(Integer id) {
        SysMessages messagesById = sysMessagesMapper.getMessagesById(id);
        Assert.isNull(messagesById.getId() == 0,"参数缺失(id is Null)");
        return sysMessagesMapper.getMessagesById(id);
    }

    @Override
    public SysMessages getMessagesByUUID(String uuid) {
        SysMessages messagesByUUID = sysMessagesMapper.getMessagesByUUID(uuid);
        Assert.isNull(messagesByUUID.getUuid() == null,"参数缺失(UUID is Null)");
        return sysMessagesMapper.getMessagesByUUID(uuid);
    }

    @Override
    public R getRandomMessages(String key,String type) {
//        List<SysMessages> messages = sysMessagesMapper.selectList(null);
        Integer exist = sysKeyMapper.selectByKey(key);
        Assert.isNull(key,"参数缺失(Key is Null)");
        Assert.verify(exist == null,"密钥错误(key is error)");
        if (type != null){
            Assert.verify("".equalsIgnoreCase(type),"参数缺失(Type is Null),或者您可以删掉Type属性");
            return R.ok().data("msg",sysMessagesMapper.getRandomMessagesByType(type));
        }
//        SysUserOrKey sysUserOrKey = sysKeyMapper.selectByUserInfo(key);

        return R.ok().data("msg",sysMessagesMapper.getRandomMessages());
    }

    @Override
    public R getPage(@Param("page") Integer page,@Param("limit") Integer limit) {
        Page<SysMessages> page1 = new Page<>(page,limit);
        QueryWrapper<SysMessages> wrapper = new QueryWrapper<>();
        Page<SysMessages> messagesPage = sysMessagesMapper.selectPage(page1, wrapper);
        long total = messagesPage.getTotal();
        List<SysMessages> records = messagesPage.getRecords();
        return R.ok().data("msg",records).data("total",total);
    }

    @Override
    public SysMessages getPageByDev(Integer offset, Integer limit) {
        return sysMessagesMapper.getPageByDev(offset,limit);
    }


}
