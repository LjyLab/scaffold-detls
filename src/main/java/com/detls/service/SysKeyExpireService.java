package com.detls.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.detls.mapper.SysKeyExpireMapper;
import com.detls.entity.SysKeyExpire;

@Service
public class SysKeyExpireService {

    @Resource
    private SysKeyExpireMapper sysKeyExpireMapper;


    public int deleteByPrimaryKey(Integer id) {
        return sysKeyExpireMapper.deleteByPrimaryKey(id);
    }


    public int insert(SysKeyExpire record) {
        return sysKeyExpireMapper.insert(record);
    }


    public int insertSelective(SysKeyExpire record) {
        return sysKeyExpireMapper.insertSelective(record);
    }


    public SysKeyExpire selectByPrimaryKey(Integer id) {
        return sysKeyExpireMapper.selectByPrimaryKey(id);
    }


    public int updateByPrimaryKeySelective(SysKeyExpire record) {
        return sysKeyExpireMapper.updateByPrimaryKeySelective(record);
    }


    public int updateByPrimaryKey(SysKeyExpire record) {
        return sysKeyExpireMapper.updateByPrimaryKey(record);
    }

}

