/*
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.detls.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.detls.config.result.R;
import com.detls.entity.SysMessages;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysMessagesService extends IService<SysMessages> {

    SysMessages getRandomMessagesByType(String type);

    SysMessages getMessagesById(Integer id);

    SysMessages getMessagesByUUID(String uuid);

    R getRandomMessages(String key,String type);

    R getPage(Integer page,Integer limit);

    SysMessages getPageByDev(@Param("offset") Integer offset, @Param("limit") Integer limit);
}
