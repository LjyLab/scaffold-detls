package com.detls.util;

import com.detls.entity.SysUser;
import com.detls.vo.SysUserVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CommonMapper {

    CommonMapper INSTANCE = Mappers.getMapper(CommonMapper.class);

    SysUserVO userToUserVo(SysUser sysUser);
}
