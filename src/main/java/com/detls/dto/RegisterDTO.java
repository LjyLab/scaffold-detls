package com.detls.dto;


import lombok.Data;

import java.io.Serializable;
@Data
public class RegisterDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 登录账号
     */
    private String userName;
    /**
     * 真实姓名
     */
    private String realName;
    /**
     * 密码
     */
    private String passWord;

    private String key;

    private String code;
}
