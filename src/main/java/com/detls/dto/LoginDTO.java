package com.detls.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class LoginDTO {

    private static final long serialVersionUID = 1L;

    /**
     * 登录账号
     */
    private String userName;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 密码
     */
    private String passWord;

    /**
     * md5密码盐
     */
    private String salt;

    /**
     * 删除状态（0，正常，1已删除）
     */
    private String delFlag;

    private String key;
    /**
     * 验证码
     */
    private String code;

}
