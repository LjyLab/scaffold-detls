package com.detls.config.exception;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;

/**
 * 数据校验
 * @author Tang
 */
public abstract class Assert {

    /**
     * 是否为null/“”/“ ”
     * @param str 字符串
     * @param message 错误提示信息
     */
    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new RunException(message);
        }
    }

    /**
     * 是否为null
     * @param object 对象
     * @param message 错误提示信息
     */
    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new RunException(message);
        }
    }

    /**
     * 是否符合条件
     * @param bool 条件
     * @param message 错误提示信息
     */
    public static void verify(boolean bool, String message) {
        if (bool) {
            throw new RunException(message);
        }
    }
}
