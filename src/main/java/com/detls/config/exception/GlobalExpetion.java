//package com.detls.config.exception;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import com.detls.config.result.Result;
//import org.apache.shiro.authz.AuthorizationException;
//import org.apache.shiro.authz.UnauthenticatedException;
//import org.apache.shiro.authz.UnauthorizedException;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//import org.springframework.web.servlet.ModelAndView;
//
//
//@ControllerAdvice
//public class GlobalExpetion {
//
//
//    public static final String DEFAULT_ERROR_VIEW = "error/error";
//    @ExceptionHandler(Exception.class)
//    @ResponseBody
//    public Object errorHandler(HttpServletRequest request,
//                               HttpServletResponse response, Exception ex) {
//        ex.printStackTrace();
//        if(isAjax(request)){
//            return ajaxException(ex, request, response);
//        }
//        else {
//            //不是异步请求
//            return exceptionPD(ex,request);
//        }
//
//    }
//    public boolean isAjax(HttpServletRequest request) {
//        return (request.getHeader("X-Requested-With") != null &&
//                "XMLHttpRequest".equals(request.getHeader("X-Requested-With").toString()));
//    }
//    public  ModelAndView  exceptionPD(Exception ex,HttpServletRequest request) {
//        ModelAndView modelAndView=new ModelAndView();
//        if(ex instanceof UnauthenticatedException) {
//            modelAndView.addObject("msg", "认证时间已过,请重新登录");
//            modelAndView.addObject("login",0);
//        }
//        else if(ex instanceof UnauthorizedException) {
//            modelAndView.addObject("msg", "你还没有该访问权限！！！请联系管理员吧");
//        }
//        else if(ex instanceof Exception){
//            modelAndView.addObject("msg", "系统异常");
//
//        }
//        modelAndView.setViewName(DEFAULT_ERROR_VIEW);
//        return modelAndView;
//    }
//
//    public Result ajaxException(Exception ex, HttpServletRequest request, HttpServletResponse response) {
//        Result<Object> json = new Result<>();
//        if(ex instanceof UnauthenticatedException) {
//            json.setCode(401);
//            json.error500("未认证或者session过期");
//            Map<String, Object> map=new HashMap<>();
//            map.put("title","是否重新登录");
//            map.put("url","/login");
//        }
//        else if(ex instanceof UnauthorizedException) {
//            json.error500("没有权限");
//            json.setCode(403);
//            Map<String, Object> map=new HashMap<>();
//            map.put("title","是否切换账户，重新登录");
//            map.put("url","/login");
//        }
//        else if(ex instanceof Exception){
//            json.setCode(500);
//            json.error500("系统异常");
//        }
//
//        return json;
//    }
//
//
//}
