package com.detls.config.exception;

import com.detls.config.result.R;
import com.detls.config.result.Result;
import com.detls.config.timeout.TakeCount;
import org.apache.shiro.authc.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.security.auth.message.AuthException;

@RestControllerAdvice
public class MyGlobalExceptionHandler {

//    @ExceptionHandler(AuthenticationException.class)
//    @ResponseBody
//    public Result shiroAuthFail(AuthException e) {
//        System.out.println("AuthException e");
//        return new Result().error500(e.getMessage());
//    }

    @TakeCount
    @ExceptionHandler(AuthenticationException.class)
//    @ResponseBody
    public R shiroAuthFail(AuthException e) {
        return R.error().message("非法访问").data("ErrorMessage",e.getMessage());
    }

//    @ExceptionHandler(RunException.class)
//    public Result runException(RunException e){
//        e.printStackTrace();
//        return new Result().error500(e.getMessage());
//    }

    @TakeCount
    @ExceptionHandler(RunException.class)
    public R runException(RunException e){
        e.printStackTrace();
        return R.error().data("ErrorMessage",e.getMessage());
    }

//    @ExceptionHandler(Exception.class)
//    public Result exception(Exception e){
//        e.printStackTrace();
//        return new Result().error500(e.getMessage());
//    }

    @TakeCount
    @ExceptionHandler(Exception.class)
    public R exception(Exception e){
        e.printStackTrace();
        return R.error().data("ErrorMessage",e.getMessage());
    }
}
