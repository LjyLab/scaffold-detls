package com.detls.config.exception;

import lombok.Data;

@Data
public class RunException extends RuntimeException{

    private static final long serialVersionUID = -9117893526280812218L;

    private String msg;

    public RunException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public RunException(String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
    }
}