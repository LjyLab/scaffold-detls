package com.detls.config.result;

public interface ResultCode {
    Integer SUCCESS = 200;
    Integer FALSE = 400;
}

