//package com.detls.config;
//
//import org.springframework.boot.web.server.ErrorPage;
//import org.springframework.boot.web.server.ErrorPageRegistrar;
//import org.springframework.boot.web.server.ErrorPageRegistry;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpStatus;
//
//@Configuration
//public class ErrorConfig implements ErrorPageRegistrar {
//
//    @Override
//    public void registerErrorPages(ErrorPageRegistry registry) {
//        ErrorPage errorPage_404 = new ErrorPage(HttpStatus.NOT_FOUND,"/error_404");
//        ErrorPage errorPage_500 = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR,"/error_500");
//        ErrorPage errorPage_405 = new ErrorPage(HttpStatus.METHOD_NOT_ALLOWED,"/error_405");
//
//        registry.addErrorPages(errorPage_404, errorPage_500,errorPage_405);
//    }
//}
