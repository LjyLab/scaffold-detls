/*
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.detls.ctrl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONObject;
import com.detls.config.constant.CacheConstant;
import com.detls.config.exception.Assert;
import com.detls.config.result.R;
import com.detls.config.timeout.TakeCount;
import com.detls.dto.ModifyExpireDTO;
import com.detls.entity.SysKeyExpire;
import com.detls.mapper.SysKeyExpireMapper;
import com.detls.mapper.SysKeyMapper;
import com.detls.util.RedisUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.Date;

@RestController
@RequestMapping("/key")
@RequiredArgsConstructor
@Slf4j
public class KeyController {
//    @Resource
    private final StringRedisTemplate stringRedisTemplate;

    private final SysKeyExpireMapper sysKeyExpireMapper;


    @TakeCount
    @GetMapping("/queryMasterRedisInfo")
    public R RobotQueryMasterRedisInfo(String key){
        SysKeyExpire sysKeyExpire = sysKeyExpireMapper.selectByKeyExpireInfo(key);
        Assert.isNull(sysKeyExpire,"KEY不存在");
        Integer quota = Integer.valueOf(stringRedisTemplate.opsForValue().get(CacheConstant.KEY_QUOTA + key));
        return R.ok().data("quota",quota).data("info",sysKeyExpire);
    }

    @TakeCount
    @GetMapping("/queryMasterInfo")
    public R RobotQueryMasterInfo(String key){
        SysKeyExpire sysKeyExpire = sysKeyExpireMapper.selectByKeyExpireInfo(key);
        Assert.isNull(sysKeyExpire,"KEY不存在");
        return R.ok().data("info",sysKeyExpire);
    }

    @TakeCount
    @PostMapping("/modifyQuota")
    public R modifiedQuota(@RequestBody ModifyExpireDTO dto){
        //MySQL不做修改
//        sysKeyExpireMapper.updateQuotaByKey(dto.getKey(),dto.getNum());
        stringRedisTemplate.opsForValue().set(CacheConstant.KEY_QUOTA + dto.getKey(), dto.getNum().toString());
        return R.ok().data("MySQL_QUOTA",sysKeyExpireMapper.getQuotaByKey(dto.getKey()))
                .data("Redis_QUOTA",stringRedisTemplate.opsForValue().get(CacheConstant.KEY_QUOTA + dto.getKey()));
    }

    @TakeCount
    @PostMapping("/modifyQuotaAdd")
    public R modifiedQuotaAdd(@RequestBody ModifyExpireDTO dto){
        Assert.verify(stringRedisTemplate.opsForValue().get(CacheConstant.KEY_QUOTA + dto.getKey()) == null,"KEY不存在");
        Integer oldQuota = Integer.valueOf(stringRedisTemplate.opsForValue().get(CacheConstant.KEY_QUOTA + dto.getKey()));
        Integer newQuota = Integer.valueOf(oldQuota + dto.getNum());
        sysKeyExpireMapper.updateQuotaByKey(dto.getKey(), newQuota);
        stringRedisTemplate.opsForValue().set(CacheConstant.KEY_QUOTA + dto.getKey(), newQuota.toString());
        log.info(String.valueOf(newQuota));
        return R.ok().data("apiMsg","Key额度充值接口")
                .data("oldQuota","正在充值Key: " + dto.getKey() + " 原有额度: " + oldQuota)
                .data("newQuota","现有额度: " + stringRedisTemplate.opsForValue().get(CacheConstant.KEY_QUOTA + dto.getKey()));
    }

    @TakeCount
    @PostMapping("/modifyQuotaCut")
    public R modifiedQuotaCut(@RequestBody ModifyExpireDTO dto){
        Assert.verify(stringRedisTemplate.opsForValue().get(CacheConstant.KEY_QUOTA + dto.getKey()) == null,"KEY不存在");
        Integer oldQuota = Integer.valueOf(stringRedisTemplate.opsForValue().get(CacheConstant.KEY_QUOTA + dto.getKey()));
        Integer newQuota = Integer.valueOf(oldQuota - dto.getNum());
        sysKeyExpireMapper.updateQuotaByKey(dto.getKey(), newQuota);
        stringRedisTemplate.opsForValue().set(CacheConstant.KEY_QUOTA + dto.getKey(), newQuota.toString());
        log.info(String.valueOf(newQuota));
        return R.ok().data("oldQuota",oldQuota)
                .data("newQuota",stringRedisTemplate.opsForValue().get(CacheConstant.KEY_QUOTA + dto.getKey()));
    }

    @TakeCount
    @PostMapping("/modifyExpireTime")
    public R modifyExpireTime(@RequestBody @Valid ModifyExpireDTO dto){
        String oldTime = sysKeyExpireMapper.selectByExpireDate(dto.getKey());
        Date thisTime = DateUtil.parse(oldTime);
        Calendar instance = Calendar.getInstance();
        instance.setTime(thisTime);
        instance.add(Calendar.DATE,dto.getNum());
        Date date = instance.getTime();
        String format = DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
        sysKeyExpireMapper.updateExpireByKey(dto.getKey(),format);
        return R.ok();
    }


    @TakeCount
    @GetMapping("/auth/{key}")
    public R authKey(@PathVariable String key){
        Integer quota = sysKeyExpireMapper.selectQuotaByKey(key);
        if (quota == null){
            return R.error().data("tips","该KEY未在系统中激活");
        }
        if (stringRedisTemplate.opsForValue().get(CacheConstant.KEY_QUOTA + key) == null){
            stringRedisTemplate.opsForValue().set(CacheConstant.KEY_QUOTA + key, String.valueOf(quota));
            sysKeyExpireMapper.RemoveByConfiguration(key);
            return R.ok().data("tips","系统查询到该KEY未同步，现已同步");
        }
        Integer oldQuota = Integer.valueOf(stringRedisTemplate.opsForValue().get(CacheConstant.KEY_QUOTA + key));
        stringRedisTemplate.opsForValue().set(CacheConstant.KEY_QUOTA + key, String.valueOf(quota + oldQuota));
        sysKeyExpireMapper.RemoveByConfiguration(key);
        return R.ok().data("tips","操作完成");
    }

    @TakeCount
    @GetMapping("/get")
    public R get(){
        JSONObject jsonObject = RedisUtils.objFromRedis(CacheConstant.KEY_QUOTA + "api_count");
        Object DomainGetIcp = jsonObject.get(CacheConstant.KEY_CONTROLLER + "ApiAllControllerDomainGetIcp");
        Object DomainGetIpV2 = jsonObject.get(CacheConstant.KEY_CONTROLLER + "ApiAllControllerDomainGetIcpV2");
        return R.ok().data("DomainGetIcp",DomainGetIcp)
                .data("DomainGetIpV2",DomainGetIpV2);
    }
}
