/*
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.detls.ctrl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.detls.config.constant.CacheConstant;
import com.detls.config.exception.Assert;
import com.detls.config.exception.ValidatorUtil;
import com.detls.config.result.R;
import com.detls.config.timeout.TakeCount;
import com.detls.entity.SysKeyExpire;
import com.detls.mapper.SysKeyExpireMapper;
import com.detls.mapper.SysKeyMapper;
import com.detls.util.SaltUtil;
import com.detls.vo.SysKeyExpireVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@RestController
@RequestMapping("/robot")
@Slf4j
public class RobotController {

    @Autowired
    private SysKeyExpireMapper sysKeyExpireMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
//    @TakeCount

    @TakeCount
    @PostMapping("/register")
    public R RobotRegister(@RequestBody SysKeyExpire sysKeyExpire){
        ValidatorUtil.validateEntity(sysKeyExpire);
        Assert.isNull(sysKeyExpire.getName(),"用户名不能为空");
        Assert.isNull(sysKeyExpire.getMasterqq(),"主人QQ不能为空");
        Assert.isNull(sysKeyExpire.getRobotqq(),"机器人QQ不能为空");
        Assert.verify(sysKeyExpireMapper.getUserByName(sysKeyExpire.getName()) != null,"用户名已存在");
        Assert.verify(sysKeyExpireMapper.getMasterByQQ(sysKeyExpire.getMasterqq()) != null,"主人QQ已被注册");
        Assert.verify(sysKeyExpireMapper.getRobotByQQ(sysKeyExpire.getRobotqq()) != null,"机器人QQ已被绑定");
        //判断生成的是什么类型的卡密 弃用
        sysKeyExpire.setKey(SaltUtil.getLetterPswSalt("",18));
        //获取当前时间
//        DateTime date = DateUtil.date().setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
        DateTime date = DateUtil.date();
        sysKeyExpire.setStart(date);
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        //免费三天
        instance.add(Calendar.DATE,3);
        Date addTime = instance.getTime();
        sysKeyExpire.setExpire(addTime);
        //免费1000额度
        sysKeyExpire.setQuota(Integer.valueOf(1000));
        int insertSelective = sysKeyExpireMapper.insertSelective(sysKeyExpire);
        Assert.verify(insertSelective == 0,"注册失败");
        return R.ok().message("注册成功").data("info",sysKeyExpire);
    }

    @TakeCount
    @GetMapping("/RobotRemoveBindingKey")
    public R RobotRemoveBindingKey(String master, String newMaster){
        Integer integer = sysKeyExpireMapper.robotRemoveBindingKey(newMaster, master);
        Assert.verify(integer == null,"修改失败");
        return R.ok().data("info",sysKeyExpireMapper.getRobotByMasterQQ(newMaster));
    }

    @TakeCount
    @GetMapping("/RobotDeleteBindingKey")
    public R RobotDeleteBindingKey(String key){
        int delete = sysKeyExpireMapper.deleteByExpireKey(key);
        stringRedisTemplate.delete(CacheConstant.KEY_QUOTA + key);
        Assert.verify(delete == 0,"删除失败");
        return R.ok().data("tips","已删除: " + key + " 无法恢复");
    }


}
