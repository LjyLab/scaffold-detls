package com.detls.mapper;

import com.detls.entity.SysLabel;

public interface SysLabelMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysLabel record);

    int insertSelective(SysLabel record);

    SysLabel selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysLabel record);

    int updateByPrimaryKey(SysLabel record);
}