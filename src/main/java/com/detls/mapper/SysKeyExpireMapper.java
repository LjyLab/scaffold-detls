package com.detls.mapper;

import com.detls.entity.SysKeyExpire;import com.detls.vo.SysKeyVO;import org.apache.ibatis.annotations.Param;

public interface SysKeyExpireMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysKeyExpire record);

    int insertSelective(SysKeyExpire record);

    SysKeyExpire selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysKeyExpire record);

    int updateByPrimaryKey(SysKeyExpire record);

    //查询用户名 是否存在
    SysKeyExpire getUserByName(@Param("username") String username);

    //查询主人QQ是否存在
    Integer getMasterByQQ(@Param("master") String master);

    Integer getRobotByQQ(@Param("master") String master);

    //解绑 -- 通过主人QQ更新机器人QQ
    Integer robotRemoveBindingKey(@Param("master") String newMaster,@Param("newMaster") String master);

    SysKeyExpire getRobotByMasterQQ(@Param("master") String master);

    Integer getQuotaByKey(String key);

    void updateQuotaByKey(@Param("key") String key,@Param("quota")  Integer quota);

    void updateExpireByKey(@Param("key") String key,@Param("expire") String expire);

    Integer updateByKeyID(Integer keyid);

    SysKeyVO selectByKeyInfo(String key);

    SysKeyExpire selectByKeyExpireInfo(String key);

    String selectByExpireDate(String key);

    SysKeyExpire robotBindingKey(String key);

    void RemoveByConfiguration(String key);

    Integer selectQuotaByKey(String key);

    Integer selectByKey(String key);

    int deleteByExpireKey(String key);
}