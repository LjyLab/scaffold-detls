package com.detls.mapper;

import com.detls.dto.SysUserOrKey;
import com.detls.entity.SysKey;
import com.detls.vo.SysKeyVO;
import com.detls.vo.SysUserKeyVO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface SysKeyMapper {

    SysUserOrKey selectByUserInfo(String key);

    Integer selectByKey(String key);

    String selectByExpireDate(String key);

//    SysKey selectByUserKeyInfo(String key);
    SysUserKeyVO selectByUserKeyInfo(String key);

    int deleteByPrimaryKey(Integer id);

    int insert(SysKey record);

    int insertSelective(SysKey record);

    SysKey selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysKey record);

    int updateByPrimaryKey(SysKey record);

    Integer selectQuotaByKey(String key);

    List<SysKey> selectAll();

    void updateQuotaByKey(@Param("key") String key,@Param("quota")  Integer quota);

    void updateExpireByKey(@Param("key") String key,@Param("expire") String expire);

    //同步给Redis后清除Mysql存储的次数
    void RemoveByConfiguration(@Param("key") String key);

//    SysKeyVO selectByKeyInfo(String key);
}