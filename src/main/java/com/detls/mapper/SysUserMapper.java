package com.detls.mapper;

import com.detls.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.detls.vo.SysUserVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.factory.Mappers;

/**
 * 用户表 Mapper 接口
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

//    SysUserMapper INSTANCE = Mappers.getMapper(SysUserMapper.class);
//
//    SysUserVO UserToUserVO(SysUser user);
    SysUser getUserByName(@Param("username") String username);

}
