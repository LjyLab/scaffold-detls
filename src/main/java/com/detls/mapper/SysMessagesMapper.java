package com.detls.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.detls.entity.SysMessages;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysMessagesMapper extends BaseMapper<SysMessages> {

    SysMessages getPageByDev(@Param("offset") Integer offset,@Param("limit") Integer limit);

    SysMessages getRandomMessagesByType(String type);

    SysMessages getMessagesById(Integer id);

    SysMessages getMessagesByUUID(String uuid);

    SysMessages getRandomMessages();
    int deleteByPrimaryKey(Integer id);

    int insert(SysMessages record);

    int insertSelective(SysMessages record);

    SysMessages selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysMessages record);

    int updateByPrimaryKey(SysMessages record);
}